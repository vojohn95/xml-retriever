package main

import (
	"fmt"
	"log"
	"time"

	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dbfact := "devengers:C3Ntr4L%4PL1c4T1V0@tcp(201.161.84.233:3306)/central_aplicativo"
	dbBomb := "u548444544_penindividual:Integrador1@tcp(sql205.main-hosting.eu:3306)/u548444544_penindividual"

	db, _ := gorm.Open(mysql.Open(dbfact), &gorm.Config{})
	db2, _ := gorm.Open(mysql.Open(dbBomb), &gorm.Config{})

	fmt.Printf("retrieving data from facturacion")
	//wait five seconds
	time.Sleep(5 * time.Second)
	retrieveData(db)
	fmt.Printf("retrieving data from bomb")
	time.Sleep(5 * time.Second)
	retrieveBomb(db2)
	time.Sleep(15 * time.Second)
}

func retrieveData(db *gorm.DB) {
	type AutoFacts struct {
		gorm.Model
		uuid string
		XML  string
	}

	// // Raw SQL
	// rows, err := db.Raw("select id, name from users").Rows()
	rows, err := db.Raw("select uuid, XML from auto_facts where fecha_timbrado BETWEEN ? AND ?", "2022-06-01 00:00:00", "2022-06-30 23:59:59").Rows()
	if err != nil {
		panic(err.Error())
		log.Print(err)
	}
	defer rows.Close()
	var uuid string
	var XML string
	for rows.Next() {
		rows.Scan(&uuid, &XML)

		fmt.Printf("datos encontrados = %s\n", uuid)
		file, e := os.Create("xmls/" + uuid + ".xml")
		if e != nil {
			log.Fatal(e)
		}
		file.WriteString(XML)
		//log.Println(file)
		file.Close()

	}
}

func retrieveBomb(db *gorm.DB) {
	type AutoFacts struct {
		gorm.Model
		uuid string
		XML  string
	}

	// // Raw SQL
	// rows, err := db.Raw("select id, name from users").Rows()
	rows, err := db.Raw("select uuid, XML from facts where fecha_timbrado BETWEEN ? AND ?", "2022-06-01 00:00:00", "2022-06-30 23:59:59").Rows()
	if err != nil {
		panic(err.Error())
		log.Print(err)
	}
	defer rows.Close()
	var uuid string
	var XML string
	for rows.Next() {
		rows.Scan(&uuid, &XML)

		fmt.Printf("datos encontrados = %s\n", uuid)
		file, e := os.Create("xmls/" + uuid + ".xml")
		if e != nil {
			log.Fatal(e)
		}
		file.WriteString(XML)
		log.Println(file)
		file.Close()

	}
}
